# Do median income trends over time analysis using IPUMS data
# Analogous to the analysis in gdp_analyze_msas.R

library(dplyr)
library(ggplot2)
library(Hmisc)
library(scales)
library(reshape2)

setwd('~/projects/regionineq/')

#Going to loop over MSAs vs CZs
varnames = c('msa','czone')
filenames = c('msa','cz')
cleannames = c('msaname','czname')
citynames = c('CBSA','Commuting Zone')

for(i in 1:length(varnames)){  #2){ #
  vname = varnames[i]
  fname = filenames[i]
  cname = cleannames[i]
  cityname = citynames[i]
  
  natldta = read.csv(paste('output/ipums/',fname,'/natinfo_',fname,'.csv',sep = ''))
  natldta[natldta$fullyear == 2000,'year'] = '00'
  dir.create(paste('output/ipums/',fname,'/devstats/',sep = ''))
  
  
  snames = c('hh','prime','primemp','fam','famprime','primemale','adult','adultmale') #,'primefemale','adultfemale'
  tnames = c('household','prime-age adult','employed, prime-age adult','family','prime-age family','prime-age male','adult','adult male') #,'prime-age female','adult female'
  
  for(s in 1:length(snames)) {
    sname = snames[s]
    tname = tnames[s]
    dir.create(paste('output/ipums/',fname,'/devstats/',sname,sep = ''))
    print(paste(vname,sname))
    
    #Get all years in one frame
    allyrs = NULL
    for(yr in c('80','90','00','10','15')) {
      dta =  read.csv(paste('output/ipums/',fname,'/dta/ipums_',fname,'_',sname,'_',yr,'.csv',sep = ''))
      yrn = as.numeric(gsub('a|b','',yr))
      if(yrn >= 50) yrn = 1900 + yrn
      if(yrn <50) yrn = 2000 + yrn

      dta$year = yrn
      allyrs = rbind(allyrs, dta)
    }
  
    #Replace income percentage NAs with 0 
    allyrs[,grep('pd',names(allyrs),value = T)] = replace(allyrs[,grep('pd',names(allyrs),value = T)],is.na(allyrs[,grep('pd',names(allyrs),value = T)]),0) 
    
    #Get national level data
    ndta = filter(natldta, samp == sname)
    dta = merge(allyrs, ndta[,c('fullyear','mmean','mmed','natvar','withincity')], by.x = 'year',by.y = 'fullyear',all.x = T,suffixes = c('','_nat'))
    
    dta$city = dta[,vname]
    dta$cityname = dta[,cname]

    dta$medrel = dta$mmed / dta$mmed_nat
    dta$meanrel = dta$mmean / dta$mmean_nat
    dta$logmed = log(dta$mmed)
    dta$logmean = log(dta$mmean)
    dta$medcat = cut(dta$medrel,breaks = c(0,.7,.8,.9,1.1,1.2,1.3,10000), labels = c('<70%','70-80%','80-90%','90-110%','110-120%','120-130%','>130%'))
    dta$meancat = cut(dta$meanrel,breaks = c(0,.7,.8,.9,1.1,1.2,1.3,10000), labels = c('<70%','70-80%','80-90%','90-110%','110-120%','120-130%','>130%'))
    
    #Statistics over time
    cstat = dta %>% group_by(year) %>% summarise(
      sd = sd(medrel),
      wsd = sqrt(wtd.var(medrel, weights = pop)),
      sdmean = sd(meanrel),
      wsdmean = sqrt(wtd.var(meanrel, weights = pop)),
      sdlog = sd(logmed),
      wsdlog = sqrt(wtd.var(logmed, weights = pop)),
      natvar = mean(natvar),
      withincity = mean(withincity),
      p10 = quantile(medrel,probs = .1),
      p25 = quantile(medrel,probs = .25),
      p50 = quantile(medrel,probs = .5),
      p75 = quantile(medrel,probs = .75),
      p90 = quantile(medrel,probs = .9),
      wp10 = wtd.quantile(medrel,probs = .1,weights = pop),
      wp25 = wtd.quantile(medrel,probs = .25,weights = pop),
      wp50 = wtd.quantile(medrel,probs = .5,weights = pop),
      wp75 = wtd.quantile(medrel,probs = .75,weights = pop),
      wp90 = wtd.quantile(medrel,probs = .9,weights = pop),
      natmed = mean(mmed_nat),
      natmean = mean(mmean_nat)) %>% data.frame()
    
    #Make IQR, 10-50-90 ratios, etc
    cstat$wiqr = cstat$wp75 - cstat$wp25
    cstat$iqr = cstat$p75 - cstat$p25
    cstat$d19 = cstat$p90 - cstat$p10
    cstat$d59 = cstat$p90 - cstat$p50
    cstat$d15 = cstat$p50 - cstat$p10
    cstat$wd19 = cstat$wp90 - cstat$wp10
    cstat$wd59 = cstat$wp90 - cstat$wp50
    cstat$wd15 = cstat$wp50 - cstat$wp10
    cstat$r95 = cstat$p90 / cstat$p50
    cstat$r51 = cstat$p50 / cstat$p10
    cstat$r55 = cstat$p50 / cstat$p50
    cstat$r15 = cstat$p10 / cstat$p50
    cstat$wr95 = cstat$wp90 / cstat$wp50
    cstat$wr51 = cstat$wp50 / cstat$wp10
    cstat$wr15 = cstat$wp10 / cstat$wp50
    cstat$wr55 = cstat$wp50 / cstat$wp50
    cstat$crosscity = (cstat$natvar - cstat$withincity) / cstat$natvar
    

    varis = c('sd','sdlog','sdmean','iqr','d19','d59','d15')
    varinames = c('Standard deviation','SD of log','Standard deviation of mean','Inter-quartile range','90th-10th percentile difference','90th-50th percentile difference','50th-10th percentile difference')
    types = c('','w')
    
    for(type in types){
      
      if(type == '') typname = 'unweighted by population'
      if(type == 'w') typname = 'weighted by population'
      if(type == '') typfile = 'uw'
      if(type == 'w') typfile = 'w'
      
      for(v in 1:length(varis)){
        
        vi = varis[v]
        variname = varinames[v]
        
        vari = paste(type,vi,sep = '')
        cstat$vint = cstat[,vari]
        titname =  paste(variname,'of',cityname,tname,'median income\nrelative to national median,',typname)
        
        pdf(paste('output/ipums/',fname,'/devstats/',sname,'/dev_',sname,'_',vi,'_',typfile,'.pdf',sep = ''))
        gplt = ggplot(cstat, aes(x = year, y = vint)) +
          geom_line() +
          geom_point() +
          theme_bw() +
          xlab('Year') +
          scale_y_continuous(limits = c(0,1.1*max(cstat$vint)), name = variname, labels = percent) +
          ggtitle(titname)
        print(gplt)
        dev.off()
      }
      
      #Plot weighted and unweighted percentiles over time
      titname = paste('Percentiles of',cityname,tname,'median income\ndistribution over time relative to national median,', typname)
      pvars = paste(type,'p',c(10,25,50,75,90),sep = '')
      cmelt = melt(cstat[,c('year',pvars)], id.vars = 'year')
      cmelt$vtype = '1'
      cmelt[cmelt$variable %in% paste(type,c('p25','p75'),sep = ''),'vtype'] = '2'
      cmelt[cmelt$variable %in% paste(type,c('p10','p90'),sep = ''),'vtype'] = '3'
      cmelt$vtype = factor(cmelt$vtype,levels = c('1','3','2'))
      
      pdf(paste('output/ipums/',fname,'/devstats/',sname,'/pctiles_',sname,'_',vi,'_',typfile,'.pdf',sep = ''))
      gplt = ggplot(cmelt, aes(x = year, y = value, group = variable, lty = vtype)) +
        geom_line() +
        geom_point() +
        theme_bw() +
        xlab('Year') +
        scale_linetype_discrete(breaks = c(1,2,3),labels = c('Median','25th/75th percentiles','10th/90th percentiles'),name = '') +
        scale_y_continuous(limits = c(0,1.1*max(cmelt$value)), name = "Percent of national median", labels = percent) +
        ggtitle(titname)
      print(gplt)
      dev.off()
      
      #Do 10-50 and 50-90 ratios over time
      titname = paste('90-50 and 50-10 ratios of',cityname,tname,',median income,\nover time,', typname)
      ratvars = paste(type,'r',c('55','15','95'),sep = '')
      cmelt = melt(cstat[,c('year',ratvars)], id.vars = 'year')
      
      pdf(paste('output/ipums/',fname,'/devstats/',sname,'/p159',sname,'_',vi,'_',typfile,'.pdf',sep = ''))
      gplt = ggplot(cmelt, aes(x = year, y = value, group = variable, lty = variable)) +
        geom_line() +
        geom_point() +
        theme_bw() +
        xlab('Year') +
        scale_linetype_discrete(breaks = ratvars[c(2,1,3)], labels = c('10th percentile','Median','90th percentile'),name = '') +
        scale_y_continuous(limits = c(.5,1.5), name = "Percent of median", labels = percent) +
        ggtitle(titname)
      print(gplt)
      dev.off()
      
    }
    
    #National median over time
    pdf(paste('output/ipums/',fname,'/devstats/',sname,'/natmed',sname,'.pdf',sep = ''))
    gplt = ggplot(cstat, aes(x = year, y = natmed, group = 1)) +
      geom_line() +
      geom_point() +
      theme_bw() +
      xlab('Year') +
      scale_y_continuous(limits = c(0,1.1*max(cstat$natmed)), name = '', labels = dollar) +
      ggtitle(paste('National',tname,'median income over time'))
    print(gplt)
    dev.off()
    
    #National mean over time
    pdf(paste('output/ipums/',fname,'/devstats/',sname,'/natmean',sname,'.pdf',sep = ''))
    gplt = ggplot(cstat, aes(x = year, y = natmean, group = 1)) +
      geom_line() +
      geom_point() +
      theme_bw() +
      xlab('Year') +
      scale_y_continuous(limits = c(0,1.1*max(cstat$natmean)), name = '',labels = dollar) +
      ggtitle(paste('National',tname,'mean income over time'))
    print(gplt)
    dev.off()
    
    #Proportion of variance across cities over time
    pdf(paste('output/ipums/',fname,'/devstats/',sname,'/crosscity',sname,'.pdf',sep = ''))
    gplt = ggplot(cstat, aes(x = year, y = crosscity, group = 1)) +
      geom_line() +
      geom_point() +
      theme_bw() +
      xlab('Year') +
      scale_y_continuous(limits = c(0,.1), name = '',labels = percent) +
      ggtitle(paste('Proportion of ',tname,' income variance across ',cityname,'s over time'))
    print(gplt)
    dev.off()
      
    #Now do chart of percentage in each percentile over time as check
    pcts = melt(dta[,c('year','city','pop',grep('pd',names(dta),value = T))],id.vars = c('year','city','pop'))
    pcts$poppctile = pcts$pop * pcts$value
    natpcts = pcts %>% group_by(year,variable) %>%summarise(pop = sum(poppctile, na.rm = T)) %>% data.frame()
    natpcts = merge(natpcts, ndta[,c('fullyear','pop')], by.x = 'year', by.y = 'fullyear',all.x = T, suffixes = c('','_nat'))
    natpcts$pctpop = natpcts$pop / natpcts$pop_nat
    
    #Check that population equals national pop
    natpop = natpcts %>% group_by(year) %>% summarise(npop = sum(pop),pop_nat = mean(pop_nat)) %>% data.frame()
    stopifnot(abs(natpop$pop_nat - natpop$npop)<10)
    
    #Make clean decile names
    natpcts$variable = factor(natpcts$variable, levels = unique(natpcts$variable), labels = gsub('pd','Decile ',unique(natpcts$variable)))
    
    pdf(paste('output/ipums/',fname,'/devstats/',sname,'/decilespop_',sname,'.pdf',sep = ''))
    gplt = ggplot(natpcts, aes(x = year, y = pop, group = variable)) +
      geom_area(aes(fill = variable)) +
      theme_bw()+
      scale_fill_brewer(palette = 'Set3',name = '')+
      scale_y_continuous(name = 'Population',labels = comma) +
      xlab('Year')+
      ggtitle(paste('Population in each decile of the',cityname,tname,'\nincome distribution over time'))
    print(gplt)
    dev.off()
    
    #Percentages
    pdf(paste('output/ipums/',fname,'/devstats/',sname,'/decilespct_',sname,'.pdf',sep = ''))
    gplt = ggplot(natpcts, aes(x = year, y = pctpop, group = variable)) +
      geom_area(aes(fill = variable)) +
      theme_bw()+
      scale_fill_brewer(palette = 'Set3',name = '')+
      scale_y_continuous(name = '',labels = percent) +
      xlab('Year')+
      ggtitle(paste('Percentage of population in each decile\nof the',cityname,tname,'income distribution over time'))
    print(gplt)
    dev.off()
    
    #Income cutoffs
    decut = melt(ndta[,c('fullyear','mmed',grep('d[1-9]',names(ndta),value = T))],id.vars = c('fullyear','mmed'))
    
    #Cutoffs relative to median
    decut$cutmed = decut$value / decut$mmed
    
    #Make clean decile names
    decut$variable = factor(decut$variable, levels = unique(decut$variable), labels = gsub('d','Decile ',unique(decut$variable)))
    
    #Make graph
    pdf(paste('output/ipums/',fname,'/devstats/',sname,'/decilescut_',sname,'.pdf',sep = ''))
    gplt = ggplot(filter(decut,variable != 'Decile 1'), aes(x = fullyear, y = cutmed, group = variable)) +
      geom_line(aes(lty = variable)) +
      theme_bw()+
      scale_linetype_discrete(name = '')+
      scale_y_log10(name = 'Percent of national median',labels = percent) +
      xlab('Year')+
      ggtitle(paste('Upper cutoffs for each decile of the',cityname,tname,'\nincome distribution over time as percentage of national median income'))
    print(gplt)
    dev.off()
    
    #Make graph of top 1%, 5%, 10% cutoff over time relative to median
    topcuts = melt(ndta[,c('fullyear','mmed','mmean','d9','top5','top1')], id.vars = c('fullyear','mmed','mmean'))
    topcuts$cutmed = topcuts$value / topcuts$mmed
    topcuts$cutmean = topcuts$value / topcuts$mmean
    topcuts$variable = factor(topcuts$variable, levels = c('d9','top5','top1'),labels = c('Top 10%','Top 5%','Top 1%'))
    
    pdf(paste('output/ipums/',fname,'/devstats/',sname,'/topcut_',sname,'.pdf',sep = ''))
    gplt = ggplot(topcuts, aes(x = fullyear, y = cutmed, group = variable)) +
      geom_line(aes(lty = variable)) +
      theme_bw()+
      scale_linetype_discrete(name = '')+
      scale_y_continuous(name = 'Percent of national median',labels = percent, limits = c(0, 1.1* max(topcuts$cutmed))) +
      xlab('Year')+
      ggtitle(paste('Lower cutoffs for top',cityname,tname,'\nincome categories over time as percentage of national median income'))
    print(gplt)
    dev.off()
    
    #Make graph of top 1%, 5%, 10% cutoff over time relative to mean
    pdf(paste('output/ipums/',fname,'/devstats/',sname,'/topcut_mean_',sname,'.pdf',sep = ''))
    gplt = ggplot(topcuts, aes(x = fullyear, y = cutmean, group = variable)) +
      geom_line(aes(lty = variable)) +
      theme_bw()+
      scale_linetype_discrete(name = '')+
      scale_y_continuous(name = 'Percent of national mean income',labels = percent, limits = c(0, 1.1* max(topcuts$cutmean))) +
      xlab('Year')+
      ggtitle(paste('Lower cutoffs for top',cityname,tname,'\nincome categories over time as percentage of national mean income'))
    print(gplt)
    dev.off()
    
    #Now do concentration index over time for each percentile
    dta$top20 = dta$pd10 + dta$pd9 + dta$pd11
    dta$top10 = dta$pd10 + dta$pd11
    dta$bottom20 = dta$pd1 + dta$pd2
    dta$mid60 = dta$pd3 + dta$pd4 + dta$pd5 + dta$pd6 + dta$pd7 + dta$pd8

    decnames = c(grep('pd',names(dta),value = T),'ptop5','ptop1','top20','bottom20','mid60','top10')
    cleandecs = c(gsub('pd','Decile ',grep('pd',names(dta),value =T)),'Top 5%','Top 1%','Top 20%','Bottom 20%','Middle 60%','Top 10%')
      
    #Loop through deciles
    dir.create(paste('output/ipums/',fname,'/devstats/',sname,'/concentration',sep = ''))
    for(j in 1:length(decnames)){
      jname = decnames[j]
      jclean = cleandecs[j]
      for(yr in c(1980,1990,2000,2010,2015)){
        ydta = filter(dta, year == yr)
        ydta$dec = ydta[,jname]
        ydta = ydta %>% arrange(-dec)
        ydta$decpop = ydta$pop * ydta$dec
        ydta$cumpop = cumsum(ydta$decpop)
        ydta$cumnatpop = cumsum(ydta$pop)
        ydta$cumpct = ydta$cumpop / sum(ydta$decpop)
        ydta$cumnatpct = ydta$cumnatpop / sum(ydta$pop)
        
        #Plot cumulative distribution
        pdf(paste('output/ipums/',fname,'/devstats/',sname,'/concentration/concent_',sname,'_',jname,'_',yr,'.pdf',sep = ''))
        gplt = ggplot(ydta, aes(x = 1:nrow(ydta)/nrow(ydta),y = cumpct)) +
          geom_line() +
          theme_bw()+
          scale_y_continuous(name = 'Cumulative percentage of national population',labels = percent) +
          scale_x_continuous(name = paste('Cumulative percentage of ',cityname,'s',sep = ''), labels = percent)+
          ggtitle(paste('Concentration of',yr,cityname,'population in',jclean))
        print(gplt)
        dev.off()
        
        #Plot cumulative distribution
        pdf(paste('output/ipums/',fname,'/devstats/',sname,'/concentration/concentpop_',sname,'_',jname,'_',yr,'.pdf',sep = ''))
        gplt = ggplot(ydta, aes(x = cumnatpct,y = cumpct)) +
          geom_line() +
          theme_bw()+
          scale_y_continuous(name = paste('Cumulative percentage of population in',jclean),labels = percent) +
          scale_x_continuous(name = paste('Cumulative percentage of national population'), labels = percent)+
          ggtitle(paste('Concentration of',yr,cityname,'population in',jclean))
        print(gplt)
        dev.off()
        
        #Export concentration data for multi-line plots
        yexp = select(ydta,year,city,cityname,dec,decpop,cumpop,cumpct,pop,cumnatpop,cumnatpct)
        yexp$order = row.names(yexp)
        write.csv(yexp, paste('output/ipums/',fname,'/devstats/',sname,'/concentration/condta_',sname,'_',jname,'_',yr,'.csv',sep = ''),row.names = F)
      
        #Compute concentration index and input into cstats
        conc = sum(ydta$cumpct) / nrow(ydta) 
        concpop = sum(ydta$cumpct * ydta$pop/sum(ydta$pop)) - .5
        concint = ydta %>% arrange(-dec) %>% filter(cumnatpct <=.1) %>% select(cumpct) %>% max() / ydta %>% arrange(-dec) %>% filter(cumnatpct <=.1) %>% select(cumnatpct) %>% max()
        cstat[cstat$year == yr,paste('concent_',jname,sep = '')] = conc
        cstat[cstat$year == yr,paste('concentint_',jname,sep = '')] = concint
        cstat[cstat$year == yr,paste('concentpop_',jname,sep = '')] = concpop
        
        #Compute concentration relative to nation and input
        ydta$relnat = (ydta$decpop/sum(ydta$decpop)) / (ydta$pop/sum(ydta$pop))
        
        ydta$stm = (ydta$dec - (sum(ydta$decpop)/sum(ydta$pop)))
        ydta$ptm = ydta$pop * ydta$stm
        stopifnot(abs(mean(ydta$ptm)) <.1)
        sharetomove = sum(abs(ydta$ptm))/2 / sum(ydta$decpop)

        cstat[cstat$year == yr, paste('sharetomove_',jname,sep = '')] = sharetomove
        }
    }
   
    #Export data
    write.csv(cstat,paste('output/ipums/',fname,'/devstats/devstats_',sname,'_',fname,'.csv',sep = ''),row.names = F)
    
    #Plot concentration index of various deciles over time
    condec = cstat[,c('year',grep('concent_pd',names(cstat),value = T))] %>% melt(id.vars = 'year')
    contop = cstat[,c('year','concent_pd10','concent_ptop5','concent_ptop1')] %>% melt(id.vars = 'year') 
    conrange = cstat[,c('year','concent_bottom20','concent_mid60','concent_top20','concent_ptop1')] %>% melt(id.vars = 'year') 
    conrangeint = cstat[,c('year','concentint_bottom20','concentint_mid60','concentint_top20','concentint_ptop1')] %>% melt(id.vars = 'year') 
    conrangepop = cstat[,c('year','concentpop_bottom20','concentpop_mid60','concentpop_top20','concentpop_ptop1')] %>% melt(id.vars = 'year') 
    conrangeshare = cstat[,c('year','sharetomove_bottom20','sharetomove_mid60','sharetomove_top20','sharetomove_ptop5','sharetomove_ptop1')] %>% melt(id.vars = 'year') 
      
    #Do all deciles
    condec$variable = gsub('concent_pd','Decile ',condec$variable)
    condec$variable = factor(condec$variable, levels = paste('Decile',1:10))
    pdf(paste('output/ipums/',fname,'/devstats/',sname,'/concent_dec_',sname,'.pdf',sep = ''))
    gplt = ggplot(condec, aes(x = year, y = value, group = variable)) +
      geom_line(aes(lty = variable))+
      scale_y_continuous(name = 'Concentration index',labels = percent,limits = c(0,1)) +
      xlab('Year')+
      theme_bw()+
      scale_linetype_discrete(name = '') +
      ggtitle(paste(cityname,'concentration index of',tname,'\nincome deciles over time'))
    print(gplt)
    dev.off()
    
    #Do just top 10, 5, and 1%
    contop$variable = factor(contop$variable, levels = c('concent_pd10','concent_ptop5','concent_ptop1'),labels = c('Top 10%','Top 5%','Top 1%'))
    pdf(paste('output/ipums/',fname,'/devstats/',sname,'/concent_top_',sname,'.pdf',sep = ''))
    gplt = ggplot(contop, aes(x = year, y = value, group = variable)) +
      geom_line(aes(lty = variable))+
      scale_y_continuous(name = 'Concentration index',labels = percent,limits = c(0,1)) +
      xlab('Year')+
      theme_bw()+
      scale_linetype_discrete(name = '') +
      ggtitle(paste(cityname,'concentration index of top',tname,'\nincomes over time'))
    print(gplt)
    dev.off()
    
    #Do bottom, middle, top, top 1%
    conrange$variable = factor(conrange$variable, levels = c('concent_bottom20','concent_mid60','concent_top20','concent_ptop1'),labels = c('Bottom 20%','Middle 60%','Top 20%','Top 1%'))
    pdf(paste('output/ipums/',fname,'/devstats/',sname,'/concent_range_',sname,'.pdf',sep = ''))
    gplt = ggplot(conrange, aes(x = year, y = value, group = variable)) +
      geom_line(aes(lty = variable))+
      scale_y_continuous(name = 'Concentration index',labels = percent,limits = c(0,1)) +
      xlab('Year')+
      theme_bw()+
      scale_linetype_discrete(name = '') +
      ggtitle(paste(cityname,'concentration index of',tname,'\nincomes over time'))
    print(gplt)
    dev.off()
    
    #Do bottom, middle, top, top 1% by population
    conrangepop$variable = factor(conrangepop$variable, levels = c('concentpop_bottom20','concentpop_mid60','concentpop_top20','concentpop_ptop1'),labels = c('Bottom 20%','Middle 60%','Top 20%','Top 1%'))
    pdf(paste('output/ipums/',fname,'/devstats/',sname,'/concentpop_range_',sname,'.pdf',sep = ''))
    gplt = ggplot(conrangepop, aes(x = year, y = value, group = variable)) +
      geom_line(aes(lty = variable))+
      scale_y_continuous(name = 'Concentration index',labels = percent,limits = c(0,.4)) +
      xlab('Year')+
      theme_bw()+
      scale_linetype_discrete(name = '') +
      ggtitle(paste(cityname,'concentration index of',tname,'\nincomes over time, population-weighted'))
    print(gplt)
    dev.off()
    
    #Do bottom, middle, top, top 1% by overrepresentation in top 10%
    conrangeint$variable = factor(conrangeint$variable, levels = c('concentint_bottom20','concentint_mid60','concentint_top20','concentint_ptop1'),labels = c('Bottom 20%','Middle 60%','Top 20%','Top 1%'))
    pdf(paste('output/ipums/',fname,'/devstats/',sname,'/concentint_range_',sname,'.pdf',sep = ''))
    gplt = ggplot(conrangeint, aes(x = year, y = value, group = variable)) +
      geom_line(aes(lty = variable))+
      scale_y_continuous(name = 'Concentration index',labels = percent,limits = c(1,5)) +
      xlab('Year')+
      theme_bw()+
      scale_linetype_discrete(name = '') +
      ggtitle(paste('Percent of ',tname,' incomes in ',cityname,'s\ncontaining 10% of national population',sep = ''))
    print(gplt)
    dev.off()
    
    #Do bottom, middle, top, top 1% share to move
    conrangeshare$variable = factor(conrangeshare$variable, levels = c('sharetomove_bottom20','sharetomove_mid60','sharetomove_top20','sharetomove_ptop5','sharetomove_ptop1'),labels = c('Bottom 20%','Middle 60%','Top 20%','Top 5%','Top 1%'))
    pdf(paste('output/ipums/',fname,'/devstats/',sname,'/concentshare_range_',sname,'.pdf',sep = ''))
    gplt = ggplot(conrangeshare, aes(x = year, y = value, group = variable)) +
      geom_line(aes(lty = variable))+
      scale_y_continuous(name = 'Share to move',labels = percent,limits = c(0,.5)) +
      xlab('Year')+
      theme_bw()+
      scale_linetype_discrete(name = '') +
      ggtitle(paste('Percent of ',tname,'s who would have to change\n',cityname,'s to achieve evenness',sep = ''))
    print(gplt)
    dev.off()
    
    #Now do concentration graph for 1980 and 2015 for top, middle, bottom, top 1%
    for(yr in c(1980,1990,2000,2010,2015)){
    allgroups = NULL
    for(d in c('ptop1','top20','mid60','bottom20')){
      dtemp = read.csv(paste('output/ipums/',fname,'/devstats/',sname,'/concentration/condta_',sname,'_',d,'_',yr,'.csv',sep = ''))
      dtemp$cat = d
      allgroups = rbind(allgroups, dtemp)
    }
    # allgroups$cat = factor(allgroups$cat, levels = c())
    
    # ct20 = read.csv(paste('output/ipums/',fname,'/devstats/',sname,'/concentration/condta_',sname,'_top20_',yr,'.csv',sep = ''))
    # cm60 = read.csv(paste('output/ipums/',fname,'/devstats/',sname,'/concentration/condta_',sname,'_mid60_',yr,'.csv',sep = ''))
    # cb20 = read.csv(paste('output/ipums/',fname,'/devstats/',sname,'/concentration/condta_',sname,'_bottom20_',yr,'.csv',sep = ''))
    # ct1 = read.csv(paste('output/ipums/',fname,'/devstats/',sname,'/concentration/condta_',sname,'_ptop1_',yr,'.csv',sep = ''))
    
    #Plot cumulative distribution
    pdf(paste('output/ipums/',fname,'/devstats/',sname,'/concentgroups_',sname,'_',yr,'.pdf',sep = ''))
    gplt = ggplot() +
      geom_line(data = allgroups,aes(x = cumnatpct, y = cumpct, group = cat,lty = cat)) +
      theme_bw()+
      scale_linetype_discrete(breaks = c('ptop1','top20','mid60','bottom20'),labels = c('Top 1%','Top 20%','Middle 60%','Bottom 20%'),name = '') +

      scale_y_continuous(name = paste('Cumulative percentage of income group population'),labels = percent) +
      scale_x_continuous(name = paste('Cumulative percentage of national population'), labels = percent)+
      ggtitle(paste('Concentration of',yr,cityname,'population in selected income groups'))
    print(gplt)
    dev.off()
    
    #Can also do representation relative to nation
    agrpop = allgroups %>% group_by(cat) %>% summarise(catpop = sum(decpop),natpop = sum(pop)) %>% data.frame()
    allgroups = merge(allgroups, agrpop, by = 'cat')
    allgroups$relnat = (allgroups$decpop/allgroups$catpop) / (allgroups$pop/allgroups$natpop)
    
    #Thinking about trying to quantify how many people need to move. Not working right now
    # allgroups$natpct = (allgroups$catpop/allgroups$natpop)
    # allgroups$natdif = allgroups$dec - allgroups$natpct
    # allgroups$ppldif = allgroups$natdif * allgroups$decpop
    # allgroups$ppldifpct = allgroups$ppldif / allgroups$catpop
    allgroups$reldif = abs((allgroups$relnat - 1))
    # allgroups %>% group_by(cat) %>% summarise(reldif = wtd.mean(reldif, weights = decpop)/2)
    
    pdf(paste('output/ipums/',fname,'/devstats/',sname,'/concentrelnat_',sname,'_',yr,'.pdf',sep = ''))
    gplt = ggplot() +
      geom_line(data = allgroups,aes(x = cumnatpct, y = relnat, group = cat,lty = cat)) +
      theme_bw()+
      scale_linetype_discrete(breaks = c('ptop1','top20','mid60','bottom20'),labels = c('Top 1%','Top 20%','Middle 60%','Bottom 20%'),name = '') +
      scale_y_continuous(name = paste('Relative concentration of income group'),labels = percent,limits = c(0,2)) +
      scale_x_continuous(name = paste('Cumulative percentage of national population'), labels = percent)+
      ggtitle(paste('Concentration of',yr,cityname,'population in selected income groups'))
    print(gplt)
    dev.off()
    }
  }
}
    #Eventualy do hypothetical what SD would be if each percentile had stayed the same but cutoffs had changed
    #And if cutoffs had stayed the same but movement had changed