#Take output of ipums_percentiles and use to to a more complete version of the counterfactuals
#First bring in cities and national income stats, rbind them into two files
#Merge 

library(dplyr)
library(Hmisc)
library(maps)
library(stringr)
library(RColorBrewer)
library(scales)
library(readstata13)
library(ggplot2)
library(reshape2)
library(ggthemes)

setwd('~/projects/regionineq/')

#Read in GDP 
gdp = read.csv('output/gdp/cz/devstats/_cz_devstats.csv')
gdpgraph = gdp %>% select(year,wsd,wsdlog,wiqr,wd19) %>% melt(id.vars = 'year')

# 
# gdp80 = filter(gdp, year == 1980) %>% select(year,wsd,wsdlog,wiqr,wd19) %>% melt(id.vars = 'year')
# gdp15 = filter(gdp, year == 2015) %>% select(year,wsd,wsdlog,wiqr,wd19) %>% melt(id.vars = 'year')
# gdpgraph = merge(gdp80,gdp15,by = 'variable',suffixes = c('80','15'))
gdpgraph$variable = factor(gdpgraph$variable, levels = c('wsd','wsdlog','wiqr','wd19'),labels = c('Coef. of variation','SD of log','IQR','10-90 range'))
gdpg69 = filter(gdpgraph, year == 1975)
gdpgraph = merge(gdpgraph, gdpg69, by = 'variable',suffixes = c('','_start'))
gdpgraph$relval = gdpgraph$value / gdpgraph$value_start

pdf('output/gdp/cz/devstats/devgraph.pdf')
gplt = ggplot(gdpgraph, aes(x = year,y = relval,lty = variable)) +
  geom_line() +
  theme_bw() +
  scale_linetype_discrete(name = '') +
  scale_x_continuous(name = 'Year') +
  scale_y_continuous(label = percent, limits = c(0,max(gdpgraph$relval)*1.1),name = 'Value relative to 1975') +
  ggtitle('Measures of sigma divergence in per capita personal income\nacross commuting zones since 1975')
print(gplt)
dev.off()

#Now do family income from IPUMS
snames = c('hh','fam','famprime','prime','primemale','primefemale') #,,'work','workmale','workfemale''primemale','famwork') #prime','fam','famprime',','adult','adultmale','adultfemale','famadult) 'famwork',
tnames = c('household','family','prime-age family','prime-age adult','prime-age male','prime-age female')#,'prime-age male','working-age family')#,'adult','adult male','adult female','adult family')prime-age adult','family','prime-age family',''prime-age female',''working-age family','working age adult','working-age male','working-age female'

for(i in 1:length(snames)) {
  print(i)
  sname = snames[i]
  tname = tnames[i]
  
  faminc = read.csv('output/ipums/cz/devstats/dev_cz.csv') %>% filter(samp == sname) %>% select(year,stat,wsd,wsdlog,wiqr,wd19)
  famgr = melt(faminc,id.vars = c('year','stat'))
  famgr$stat = factor(famgr$stat, levels = c('med','mean'),labels = c('Median','Mean'))
  famgr$variable = factor(famgr$variable, levels = c('wsd','wsdlog','wiqr','wd19'),labels = c('Coef. of variation','SD of log','IQR','10-90 range'))
  fam80 = filter(famgr,year == 1980)
  famgr = merge(famgr,fam80,by = c('stat','variable'), suffixes = c('','_start'))
  famgr$relval = famgr$value / famgr$value_start
  
  pdf(paste('output/ipums/cz/devstats/',sname,'/devgraph_',sname,'.pdf',sep = ''))
  gplt = ggplot(famgr, aes(x = year,y = relval,lty = variable,alpha = stat)) +
    geom_line() +
    geom_point() +
    theme_bw() +
    # scale_
    scale_alpha_discrete(range = c(0.3,1),name = '') +
    # scale_poin
    scale_linetype_discrete(name = '') +
    scale_x_continuous(name = 'Year') +
    scale_y_continuous(label = percent, limits = c(0,max(famgr$relval)*1.1),name = 'Value relative to 1980') +
    ggtitle(paste('Measures of sigma divergence in',tname,'income\nacross commuting zones since 1980'))
  print(gplt)
  dev.off()
}
