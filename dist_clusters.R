#Try making clusters of regional income distributions

library(dplyr)
library(ggplot2)
library(stringr)
library(reshape2)
library(scales)

library(emdist)
setwd('~/projects/regionineq/')

#Earth Move Dist Function
em = function(x,y,size){	
  a = cbind(t(x),1:size)
  b = cbind(t(y),1:size)
  return(emd(a,b))
}

#Going to loop over MSAs vs CZs
varnames = c('msa','czone')
filenames = c('msa','cz')
cleannames = c('msaname','czname')
citynames = c('CBSA','commuting zone')

for(i in 1:length(varnames)){  # 2){ #  
  vname = varnames[i]
  fname = filenames[i]
  cname = cleannames[i]
  cityname = citynames[i]
  
  snames = c('hh','fam','famprime','prime','primemale','primefemale')#,,'work','workmale','workfemale''primemale','famwork') #prime','fam','famprime',','adult','adultmale','adultfemale','famadult) 'famwork',
  tnames = c('household','family','prime-age family','prime-age adult','prime-age male','prime-age female')#,'prime-age male','working-age family')#,'adult','adult male','adult female','adult family')prime-age adult','family','prime-age family',''prime-age female',''working-age family','working age adult','working-age male','working-age female'
  
  dir.create(paste('output/ipums/',fname,'/parallelcoord',sep = ''))
  
  for(s in 1:length(snames)) {
    sname = snames[s]
    tname = tnames[s]

    dta = read.csv(paste('output/ipums/',fname,'/dta/',sname,'/',fname,'_pct_city_',sname,'_2015.csv',sep = ''))
    dta$city = as.character(dta[,vname])
    dta$cityname = as.character(dta[,cname])
    dpcts = dta %>% select(city,cityname, pct, p)
    
    #Group into different sizes
    for(psize in c(1,2,4,5,10)){
      #Make grouped percentiles
      dpcts$pctgrp = ceiling(dpcts$p / psize)
      dp = dpcts %>% group_by(pctgrp,city) %>% summarise(
        pct = sum(pct)) %>% data.frame()
      
      pmat = dcast(dp, city ~ pctgrp, value.var = 'pct')
      
      #Distance Matrix
      distances = matrix(nrow = nrow(pmat), ncol = nrow(pmat))
      for(k in 1:nrow(pmat)){
        print(k)
        for(j in 1:nrow(pmat)){
          distances[k,j] = em(pmat[k,2:ncol(pmat)],pmat[j,2:ncol(pmat)],100/psize)
          
        }
      }
      
      colnames(distances) = pmn$msaname # pmat$city # 
      rownames(distances) = pmn$msaname # pmat$city # 
      emdist = as.dist(distances)
      
      clusteranalysis = hclust(emdist)
      
      clustord = rcats[clusteranalysis$order,"categories"]
      clustordmatch = match(clustord,rollup[,1])
      supclustord = rollup[,2][clustordmatch]
      supclustord[is.na(supclustord)] = as.character(clustord[is.na(supclustord)])
      
      palette( c(brewer.pal(3,"Set3"), brewer.pal(8,"Dark2"),brewer.pal(9,"Set1")))
      
      pdf(paste('output/ipums/',fname,'/parallelcoord/',sname,'/tree1.pdf',sep = ''), height = 5)
      plot(clusteranalysis, cex = .5,labels = FALSE,xlab = NA, ylab= NA, sub = NA, main="Cluster Dendrogram:\nCategories Over 25 Establishments Colored by Supercategory")
      legend(x = "topright", legend = sort(unique(supclustord)),col = 1:20,pch = 15, cex = .4)
      
      points(1:164, rep(-.3,164), col = as.factor(supclustord), pch = 15)
      
      
      dev.off()
      
      test2 = kmeans(distances,5)
      test2 = kmeans(distances,4)
      pm2 = cbind(pmat, test2$cluster)
      pmp = melt(pm2, id.vars = c('city','test2$cluster'))
      names(pmp) = c('city','cluster','variable','value')
      pmpl = pmp %>% group_by(cluster, variable) %>% summarise(value = mean(value)) %>% data.frame()
      
      pdf(paste('output/ipums/',fname,'/parallelcoord/',sname,'/kmeans4.pdf',sep = ''), height = 5)
      gplt = ggplot(pmpl, aes(x = variable, y = value, group = cluster, color = cluster)) +
        geom_line()
      print(gplt)
      dev.off()
    } #psize
  } #samp
} #cz msa