#Take output of ipums_percentiles and use to to a more complete version of the counterfactuals
#First bring in cities and national income stats, rbind them into two files
#Merge 

library(dplyr)
library(Hmisc)
library(maps)
library(stringr)
library(RColorBrewer)
library(scales)
library(readstata13)
library(ggplot2)
library(reshape2)
library(ggthemes)

setwd('~/projects/regionineq/')

#Bring in CPI
cpirs = read.dta13('~/projects/reference/cpi/cpirs.dta')
cpi15 = cpirs[cpirs$year == 2015,'rate']

#Crosswalk from MSAs to counties in the maps
msas = read.csv('data/mable/cntymsakey.csv')
msas$reg = msas$polyname

cntys = map_data('county')
cntys$reg = paste(cntys$region, cntys$subregion, sep = ',')
cntys = merge(cntys,msas,by = 'reg')

#Going to loop over MSAs vs CZs
varnames = c('msa','czone')
filenames = c('msa','cz')
cleannames = c('msaname','czname')
citynames = c('CBSA','commuting zone')

for(i in 1:length(varnames)){  # 2){ #  
  vname = varnames[i]
  fname = filenames[i]
  cname = cleannames[i]
  cityname = citynames[i]
  
  cntys$city = cntys[,vname]
  
  natldta = read.csv(paste('output/ipums/',fname,'/natinfo_',fname,'.csv',sep = ''))
  natldta$year = natldta$fullyear
  
  snames = c('hh','fam','famprime','prime','primemale','primefemale')#,,'work','workmale','workfemale''primemale','famwork') #prime','fam','famprime',','adult','adultmale','adultfemale','famadult) 'famwork',
  tnames = c('household','family','prime-age family','prime-age adult','prime-age male','prime-age female')#,'prime-age male','working-age family')#,'adult','adult male','adult female','adult family')prime-age adult','family','prime-age family',''prime-age female',''working-age family','working age adult','working-age male','working-age female'
  
  dir.create(paste('output/ipums/',fname,'/counterfactuals',sep = ''))
  
  cfsig = NULL
  cfbeta = NULL
  
  for(s in 1:length(snames)) {
    sname = snames[s]
    tname = tnames[s]
    
    dir.create(paste('output/ipums/',fname,'/counterfactuals/',sname,sep = ''))
    for(psize in c(1,2,4,5,10)){
        
        #Make dataframe to store national information from each sample
        natinfo80 = read.csv(paste('output/ipums/',fname,'/dta/',sname,'/',fname,'_pct_nat_',sname,'_1980.csv',sep = ''))
        natinfo90 = read.csv(paste('output/ipums/',fname,'/dta/',sname,'/',fname,'_pct_nat_',sname,'_1990.csv',sep = ''))
        natinfo00 = read.csv(paste('output/ipums/',fname,'/dta/',sname,'/',fname,'_pct_nat_',sname,'_2000.csv',sep = ''))
        natinfo10 = read.csv(paste('output/ipums/',fname,'/dta/',sname,'/',fname,'_pct_nat_',sname,'_2010.csv',sep = ''))
        natinfo15 = read.csv(paste('output/ipums/',fname,'/dta/',sname,'/',fname,'_pct_nat_',sname,'_2015.csv',sep = ''))
        
        natinfo_full = rbind(natinfo80,natinfo90,natinfo00, natinfo10, natinfo15)
        
        #Make city year by year data frame
        city80 = read.csv(paste('output/ipums/',fname,'/dta/',sname,'/',fname,'_pct_city_',sname,'_1980.csv',sep = ''))
        city90 = read.csv(paste('output/ipums/',fname,'/dta/',sname,'/',fname,'_pct_city_',sname,'_1990.csv',sep = ''))
        city00 = read.csv(paste('output/ipums/',fname,'/dta/',sname,'/',fname,'_pct_city_',sname,'_2000.csv',sep = ''))
        city10 = read.csv(paste('output/ipums/',fname,'/dta/',sname,'/',fname,'_pct_city_',sname,'_2010.csv',sep = ''))
        city15 = read.csv(paste('output/ipums/',fname,'/dta/',sname,'/',fname,'_pct_city_',sname,'_2015.csv',sep = ''))
        
        city_full = rbind(city80, city90, city00, city10, city15)
        
        #Add city variable
        city_full$city = city_full[,fname]
        
        stattypes = c('med','mean')
        statnames = c('median','mean')
        for(statnum in 1:length(stattypes)){
          st = stattypes[statnum]
          stname = statnames[statnum]
          print(paste(vname,sname,psize,st))
          
          #Get national mean and median 
          natlmeans = filter(natldta,samp == sname)[,c('year',paste('m',st,sep = ''))]
          names(natlmeans) = c('year','nattotstat')
          
          natinfo = natinfo_full
          city = city_full
          natinfo$stat = natinfo[,st]
          city$stat = city[,st]
          
          #Make grouped percentiles
          natinfo$pctgrp = ceil(natinfo$p / psize)
          natinfo = natinfo %>% group_by(pctgrp,year) %>% summarise(
            count = sum(cnt), pct = sum(pct), stat = wtd.mean(stat, weights=cnt,na.rm = T)) %>% data.frame()
            
          #Here too drop NAs- come up in prime male for a few percentiels that get bunched up
          #That's not working so replace with zeros I guess. Shouldn't matter because weights are zero?
          natinfo$stat = replace(natinfo$stat, is.na(natinfo$stat),0)
          
          # #Make national means for each year
          # natmeans = natinfo %>% group_by(year) %>% summarise(natmean = wtd.mean(stat, weights = count)) %>% data.frame()
          
          #Make national percentile mean relative to national total mean
          natinfo = merge(natinfo, natlmeans, by = 'year')
          natinfo$nat_pctl_rel_tot = natinfo$stat / natinfo$nattotstat
          
          
          #Make grouped city percentiles
          city$pctgrp = ceil(city$p / psize)
          city = city %>% group_by(city,pctgrp,year) %>% summarise(
            count = sum(cnt), pct = sum(pct), stat = wtd.mean(stat, weights=cnt,na.rm =T)) %>% data.frame()
          
          #I think removing NA is good because they are actually zeros. So don't wnat to average them.
          
          #Add national year and national percentile means
          # city = merge(city, natmeans, by = 'year')
          city = merge(city, natinfo[,c('year','pctgrp','stat','nat_pctl_rel_tot','nattotstat','pct')], by = c('year','pctgrp'), suffixes = c('_city','_nat'), all.x = T)
          
          #Make sure merge worked
          stopifnot(!is.na(city$stat_nat))
          
          #Replace missing stats with national stat - for now
          city[is.na(city$stat_city),'stat_city'] = city[is.na(city$stat_city),'stat_nat']
          
          #Make city percentile stat relative to national percentile stat
          city$city_pctl_rel_nat = city$stat_city / city$stat_nat
          city[city$stat_nat == 0 & city$stat_city == 0,'city_pctl_rel_nat'] = 1 #Cases where both are zero get set to 1  
          stopifnot(!is.na(city$city_pctl_rel_nat))
          
          #Add 1980 values of city pct stat relative to national pct stat, national pct stat relative to national stat, and city percent in percentile
          c1980 = filter(city, year == 1980) %>% select(city,pctgrp,nat_pctl_rel_tot,city_pctl_rel_nat,pct_city)
          city = merge(city,c1980, by = c('city','pctgrp'), suffixes = c('_XX','_80'),all.x = T)
          
          #One MSA that doesn't have data for 1980
          table(city[is.na(city$city_pctl_rel_nat_80),'city']) 
          #MSAs: 24380 - Grants, NM Micropolitan Statistical Area, which is one county that was formed after 1980. Drop.
          #CZs: 34101
          city = filter(city, !is.na(city_pctl_rel_nat_80))
          
          #Now, do two sets of counterfactual incomes
          cfacts = city %>% group_by(city,year) %>% summarise(pop = sum(count),actual_incstat = wtd.mean(stat_city,weights = count),natl_incstat = mean(stat_nat)) %>% data.frame()
          for(gcfact in c('XX','80')){
            for(icfact in c('XX','80')){
              #geography is both pct_city and city_pctle_rel_nat
              #Income is just natl_pctl_rel_tot
              city$geovar = city[,paste('pct_city_',gcfact,sep = '')]
              city$incvar = city[,paste('city_pctl_rel_nat_',gcfact,sep = '')] * city[,paste('nat_pctl_rel_tot_',icfact,sep = '')]
              
              # city[,paste('cfact_inc_g',gcfact,'_i',icfact,sep = '')] = city[,paste('pct_city_',gcfact,sep = '')] * city[,paste('city_pctl_rel_nat_',gcfact,sep = '')] * city[,paste('nat_pctl_rel_tot_',icfact,sep = '')] 
              if(st == 'mean') cf = city %>% group_by(city,year) %>% summarise(cf = wtd.mean(incvar,weights = geovar*1000)) %>% data.frame()
              if(st == 'med') cf = city %>% group_by(city,year) %>% summarise(cf = wtd.quantile(incvar,weights = geovar *1000,probs = .5)) %>% data.frame() #For some reason the weights aren't working when less than 1.
              names(cf) = c('city','year',paste('cfact_inc_g',gcfact,'_i',icfact,sep = ''))
              cfacts = merge(cfacts, cf, by = c('city','year'),all.x = T )
            }
          }
          cfacts$actual_statrel = cfacts$actual_incstat / cfacts$natl_incstat
          
          #Make list of counterfactual incomes
          # cfacts = city %>% group_by(city,year) %>% summarise(pop = sum(count),actual_incstat = wtd.mean(stat_city,weights = count),natl_incstat = mean(stat_nat)
          #                                                     
          #                                                     
          #                                                     ,cfact_inc_gXX_iXX = sum(cfact_inc_gXX_iXX),
          #           cfact_inc_g80_i80 = sum(cfact_inc_g80_i80),cfact_inc_g80_iXX = sum(cfact_inc_g80_iXX),cfact_inc_gXX_i80 = sum(cfact_inc_gXX_i80)) %>%data.frame()
          #              
          write.csv(cfacts,paste('output/ipums/',fname,'/counterfactuals/',sname,'/cf_',fname,'_',st,'_',psize,'.csv',sep = ''),row.names = F)           
             
          #Make sd relative to stat over time
          cfactsds = cfacts %>% group_by(year) %>% summarise(
            sd_actual = sqrt(wtd.var(actual_statrel, weights = pop)),
            sd_gXX_iXX = sqrt(wtd.var(cfact_inc_gXX_iXX, weights = pop)),
            sd_g80_i80 = sqrt(wtd.var(cfact_inc_g80_i80, weights = pop)),
            sd_g80_iXX = sqrt(wtd.var(cfact_inc_g80_iXX, weights = pop)),
            sd_gXX_i80 = sqrt(wtd.var(cfact_inc_gXX_i80, weights = pop))
          ) %>% data.frame()
            
          #Plot
          pltdta = cfactsds %>% melt(id.vars = 'year')
          sddta = pltdta
          # pltdta$cleannames = factor(pltdta$variable, levels = paste(dvar,c('g8_i8','gX_i8','g8_iX','gX_iX'),sep = '_'), labels = c('1980 geography, 1980 income','Current geography, 1980 income','1980 geography, current income','Current geography, current income'))
          pdf(paste('output/ipums/',fname,'/counterfactuals/',sname,'/cf_',sname,'_',st,'_sd_',psize,'.pdf',sep = ''))
          gplt = ggplot(pltdta, aes(x = year, y = value, lty = variable, group = variable)) +
            geom_line() +
            geom_point() +
            scale_y_continuous(limits = c(0,1.1 * max(pltdta$value)),name = paste('SD of',cityname,stname,'income relative to national',stname)) +
            theme_bw() +
            ggtitle(paste('Counterfactual variation in standard deviation of',cityname,stname,'incomes'))
          print(gplt)
          dev.off()
          
          #Make 10-90 difference relative to mean over time
          cfactpcts = cfacts %>% group_by(year) %>% summarise(
            p10_actual = wtd.quantile(actual_statrel,weights = pop, probs = .1),
            p10_gXX_iXX = wtd.quantile(cfact_inc_gXX_iXX,weights = pop, probs = .1),
            p10_g80_i80 = wtd.quantile(cfact_inc_g80_i80,weights = pop, probs = .1),
            p10_g80_iXX = wtd.quantile(cfact_inc_g80_iXX,weights = pop, probs = .1),
            p10_gXX_i80 = wtd.quantile(cfact_inc_gXX_i80,weights = pop, probs = .1),
            p25_actual = wtd.quantile(actual_statrel,weights = pop, probs = .25),
            p25_gXX_iXX = wtd.quantile(cfact_inc_gXX_iXX,weights = pop, probs = .25),
            p25_g80_i80 = wtd.quantile(cfact_inc_g80_i80,weights = pop, probs = .25),
            p25_g80_iXX = wtd.quantile(cfact_inc_g80_iXX,weights = pop, probs = .25),
            p25_gXX_i80 = wtd.quantile(cfact_inc_gXX_i80,weights = pop, probs = .25),
            p75_actual = wtd.quantile(actual_statrel,weights = pop, probs = .75),
            p75_gXX_iXX = wtd.quantile(cfact_inc_gXX_iXX,weights = pop, probs = .75),
            p75_g80_i80 = wtd.quantile(cfact_inc_g80_i80,weights = pop, probs = .75),
            p75_g80_iXX = wtd.quantile(cfact_inc_g80_iXX,weights = pop, probs = .75),
            p75_gXX_i80 = wtd.quantile(cfact_inc_gXX_i80,weights = pop, probs = .75),
            p90_actual = wtd.quantile(actual_statrel,weights = pop, probs = .9),
            p90_gXX_iXX = wtd.quantile(cfact_inc_gXX_iXX,weights = pop, probs = .9),
            p90_g80_i80 = wtd.quantile(cfact_inc_g80_i80,weights = pop, probs = .9),
            p90_g80_iXX = wtd.quantile(cfact_inc_g80_iXX,weights = pop, probs = .9),
            p90_gXX_i80 = wtd.quantile(cfact_inc_gXX_i80,weights = pop, probs = .9)
          ) %>% data.frame()
          
          for(v in c('actual','gXX_iXX','g80_i80','g80_iXX','gXX_i80')){
            cfactpcts[,paste('iqr_',v,sep = '')] = cfactpcts[,paste('p75_',v,sep = '')] -cfactpcts[,paste('p25_',v,sep = '')]  
            cfactpcts[,paste('p19_',v,sep = '')] = cfactpcts[,paste('p90_',v,sep = '')] -cfactpcts[,paste('p10_',v,sep = '')]  
          }
          
          #Plot IQR
          pltdta = cfactpcts[,c('year',grep('iqr',names(cfactpcts),value = T))]
          pltdta = pltdta %>% melt(id.vars = 'year')
          iqrdta = pltdta
          # pltdta$cleannames = factor(pltdta$variable, levels = paste(dvar,c('g8_i8','gX_i8','g8_iX','gX_iX'),sep = '_'), labels = c('1980 geography, 1980 income','Current geography, 1980 income','1980 geography, current income','Current geography, current income'))
          pdf(paste('output/ipums/',fname,'/counterfactuals/',sname,'/cf_',sname,'_',st,'_iqr_',psize,'.pdf',sep = ''))
          gplt = ggplot(pltdta, aes(x = year, y = value, lty = variable, group = variable)) +
            geom_line() +
            geom_point() +
            scale_y_continuous(limits = c(0,1.1 * max(pltdta$value)),name = paste('IQR of',cityname,stname,'income relative to national',stname)) +
            theme_bw() +
            ggtitle(paste('Counterfactual variation in IQR of',cityname,stname,tname,'incomes'))
          print(gplt)
          dev.off()
          
          #Plot 90-10 range
          pltdta = cfactpcts[,c('year',grep('p19',names(cfactpcts),value = T))]
          pltdta = pltdta %>% melt(id.vars = 'year')
          p91dta = pltdta
          # pltdta$cleannames = factor(pltdta$variable, levels = paste(dvar,c('g8_i8','gX_i8','g8_iX','gX_iX'),sep = '_'), labels = c('1980 geography, 1980 income','Current geography, 1980 income','1980 geography, current income','Current geography, current income'))
          pdf(paste('output/ipums/',fname,'/counterfactuals/',sname,'/cf_',sname,'_',st,'_p19_',psize,'.pdf',sep = ''))
          gplt = ggplot(pltdta, aes(x = year, y = value, lty = variable, group = variable)) +
            geom_line() +
            geom_point() +
            scale_y_continuous(limits = c(0,1.1 * max(pltdta$value)),name = paste('90-10 range of',cityname,stname,'income relative to national',stname)) +
            theme_bw() +
            ggtitle(paste('Counterfactual variation in 90-10 range of',cityname,stname,tname,'incomes'))
          print(gplt)
          dev.off()
          
          #Output data frame of the various statistics 
          stdta = rbind(sddta,iqrdta,p91dta)
          stdta = dcast(stdta, year ~ variable,value.var = 'value')
          stdta$samp = sname
          stdta$stat = st
          
          cfsig = rbind(cfsig, stdta)
          
          #Maps
          incnames = c('1980 income at each percentile',paste(2015,'income at each percentile'))
          geonames = c('1980 geographic distribution',paste(2015,'geographic distribution'))
          incyears = c('80','XX')
          for(inum in 1:length(incyears)){
            for(gnum in 1:length(incyears)){
              
              inu = incyears[inum]
              gnu = incyears[gnum]
              iname = incnames[inum]
              gname = geonames[gnum]
              
              cfacts$varint = cfacts[,paste('cfact_inc_g',gnu,'_i',inu,sep = '')]
              
              cfacts$varcat = cut(cfacts$varint,breaks = c(0,.7,.8,.9,1.1,1.2,1.3,10000), labels = c('<70%','70-80%','80-90%','90-110%','110-120%','120-130%','>130%'))
              
              cfacts15 = filter(cfacts, year == 2015)
              mapdat = merge(cntys,cfacts15,by = 'city', all.x = T)
              
              stopifnot(!is.na(mapdat$varcat) | mapdat$city == '24380') #Grants, NM, which is a 1-county MSA that didn't use to exist
              
              #Map data
              mapdat = mapdat %>% arrange(order)
              
              maptit = paste('Simulated',cityname,stname,tname,'income\n',iname,'-',gname,'\npercentile group size:',psize )
              
              #Categorical map
              pdf(paste('output/ipums/',fname,'/counterfactuals/',sname,'/cf_',sname,'_',st,'map_g',gnu,'_i',inu,'_',psize,'.pdf',sep = ''))
              gplot = ggplot(data = mapdat, aes(long, lat, group = group)) +
                geom_polygon( aes(fill = varcat)) +
                coord_map("polyconic") +
                scale_fill_manual(values = colorRampPalette(c('darkgreen','grey60','midnightblue'))(7),name= '') +
                # scale_fill_gradient2(midpoint = 1, label = percent, name = '', limits = c(0,2), low = 'midnightblue',mid = 'grey',high = 'darkgreen') +
                # scale_fill_distiller(palette = 'PRGn',direction = 1, label = percent, name = '')+
                # (palette = 'Blue', label = percent, name = '')+
                theme_map() +
                theme(legend.position = 'bottom',legend.justification = c(.5,.5),plot.title = element_text(hjust = 0.5)) +
                guides(fill = guide_legend(ncol = 7)) +
                ggtitle(maptit)
              print(gplot)
              dev.off()
              
              #Do beta convergence
              cfacts80 = filter(cfacts, year == 1980)
              cfbeta = merge(cfacts15,cfacts80, by = 'city',suffixes = c('','_80'))
              cfbeta$inc15 = cfbeta$natl_incstat * cfbeta$varint
              cfbeta$inc80 = cfbeta$natl_incstat_80 * cfbeta$varint_80
              cfbeta$anngr = (cfbeta$inc15 / cfbeta$inc80) ^ (1/35) - 1
              
              #Fit linear model
              mod1 = lm(formula = anngr ~ inc80,weights = pop,data = cfbeta)
    
              #Title
              betatit = paste('Simulated beta convergence in',cityname,stname,tname,'income, 1980-2015\n',iname,'-',gname,'\npercentile group size:',psize )
              
              #Plot annual growth rate vs
              pdf(paste('output/ipums/',fname,'/counterfactuals/',sname,'/cf_',sname,'_',st,'beta_g',gnu,'_i',inu,'_',psize,'.pdf',sep = ''))
              gplt = ggplot(cfbeta, aes(x = inc80, y = anngr,size = pop)) +
                geom_point(alpha = .3) +
                theme_bw() +
                scale_x_continuous(labels = dollar, name = paste(cityname,stname,'income, 1980')) +
                scale_y_continuous(labels = percent, name = paste('Annualized growth rate in',stname,'income, 1980-2015')) +
                geom_abline(intercept = mod1$coefficients[1], slope = mod1$coefficients[2],lty = 2) +
                scale_size_continuous(range = c(0,5), labels = comma, name = paste(cityname, '\nPopulation')) +
                ggtitle(betatit)
              
              print(gplt)
              dev.off()
              
              
          } #inum
        } #gnum
      } #stat type
    } #psize
  } #samp
} #var