#Do a more thorough version of income at each percentile
#Calculate and output the national percentiles, doing all 100
#Figure out how to deal with zeroes: split evenly among bottom X percentiles
#For each city:
  #Calculate the percentage of population in each percentile
  #Allocate zeroes correctly
  #Calculate mean income within each national percentile
  #Calculate city median income in dollars and in terms of national percentiles
#Output:
  #National list of percentile cutoffs, total population in each percentile, mean and median income in each percentile
  #Metro list of population in each percentile, mean and median income in each percentile, 
  #Metro list of mean and median income for each metro
#Eventual goals
  #Counterfactuals where we do 1980 geography and 2015 inequality
  #Attribution of how much divergence is about the top 

library(dplyr)
library(Hmisc)
library(maps)
library(stringr)
library(RColorBrewer)
library(scales)
# library(reldist)
library(readstata13)
library(ggplot2)

setwd('~/projects/regionineq/')

#Bring in CPI
cpirs = read.dta13('~/projects/reference/cpi/cpirs.dta')
cpi15 = cpirs[cpirs$year == 2015,'rate']

#Going to loop over MSAs vs CZs
varnames = c('msa','czone')
filenames = c('msa','cz')
cleannames = c('msaname','czname')
citynames = c('MSA','Commuting Zone')

for(i in 2){ #  1:length(varnames)){  #
  vname = varnames[i]
  fname = filenames[i]
  cname = cleannames[i]
  cityname = citynames[i]
  
  #Make dataframe to store national information from each sample
  natinfo =NULL 
  
  #Loop through years
  for(yr in c('80','90','00','10','15')){ #
    fullyr = as.numeric(yr) + 2000
    if(yr %in% c('80','90')) fullyr = fullyr - 100
    print(paste(yr, vname))
    cpiyr = cpirs %>% filter(year == fullyr - 1) %>% select(rate) %>% unlist()
    
    
    dtfile = load(paste('data/ipums/ip',yr,fname,'.RData',sep = ''))
    dta = get(dtfile)
    
    #Make city identfier
    dta$city = dta[,vname]
    dta$cityname = as.character(dta[,cname])
    
    #Inflate 
    dta$hhincome = dta$hhincome * cpi15 / cpiyr
    dta$inctot = dta$inctot * cpi15 / cpiyr
    dta$ftotinc = dta$ftotinc * cpi15 / cpiyr
    
    #Do one sample that's hh income of all households
    hh = dta %>% group_by(city,cityname, serial) %>% summarise(hhincome = mean(hhincome, na.rm  =T), hhinc = sum(inctot, na.rm = T), hcwt = mean(hcwt, na.rm = T)) %>% data.frame()
    summary(hh$hhinc - hh$hhincome) # They mostly agree but not quite. Some of this could be not replacing HH inc missings with zero and some might be somethign else. The biggest discripancies appear to be problems on the precalculated side
    
    #Do one for family incomes of people of prime working age
    famprime = dta %>% filter(age >= 25, age <= 54)
    
    #Do one for all families, just to see
    fam = dta
    
    #Do one sample that's just individuals of prime working age
    prime = dta %>% filter(age >= 25, age <= 54)
    
    #Do one sample that is all men of prime working age (trying to net out female labor force participation)
    primemale = dta %>% filter(age >= 25, age <= 54, sex == 'Male')
    primefemale = dta %>% filter(age >= 25, age <= 54, sex == 'Female')
    
    #Adult family income
    # famadult = dta %>% filter(age >=18)
    
    #Working age family income
    famwork = dta %>% filter(age >= 18, age <= 64)
    work = dta %>% filter(age >= 18, age <= 64)
    workmale = dta %>% filter(age >= 18, age <= 64, sex == 'Male')
    workfemale = dta %>% filter(age >= 18, age <= 64, sex == 'Female')
    
    
    #7% of 2015 prime males had no income
    #1% of 2015 households had no income
    
    #Maybe I don't separate out  bug weight by number of people in that group in addition
    #So segregation of people with zero income is bigger
    #Or this may come out by repleating it a bunch of times, once per percentile
    
    #But if city percent zero isn't in line with nation, do you assign it to 0th or 5th percentile of nation?
    #Maybe spread evenly?
    #And then I guess don't put it into teh box that has some zeros and some not.
    
    #And then in simulations, assign the first X% of zerose to Xth percentile, adn then move on. 
    #And re-calcualte Jth percentile mean to be weighted average of that and zero.
    
    #For each percentile, get national population and national mean in that percentile, 
    #Then for each city get percentage and mean in that percentile
    
    #Output:
    #year x percentile x city file
    #Each row have 
        #percent of city in that percentile in that year
        #mean of that city in that percentile year
    
    
    #Loop through samples 
    samps = list(hh,fam,famwork,famprime,prime,primemale,primefemale,work,workmale,workfemale)#,primemale,famwork) #prime,fam,famprime,primemale,primefemale,famwork) #,adult,adultmale,adultfemale,famadult)
    snames = c('hh','fam','famwork','famprime','prime','primemale','primefemale','work','workmale','workfemale')#,'primemale','famwork') #prime','fam','famprime',','adult','adultmale','adultfemale','famadult)
    tnames = c('household','family','working-age family','prime-age family','prime-age adult','prime-age male','prime-age female','working age adult','working-age male','working-age female')#,'prime-age male','working-age family')#,'adult','adult male','adult female','adult family')prime-age adult','family','prime-age family',''prime-age female','
    
    for(s in 1:length(samps)){
      
      samp = samps[[s]]
      sname = snames[s]
      titname = tnames[s]
      print(sname)
      dir.create(paste('output/ipums/',fname,'/dta/',sname,sep = ''))
      
      #Get weights
      if(sname == 'hh') samp$wt = samp$hcwt
      if(sname %in% c('prime','primemp','fam','famprime','primemale','primefemale','adult','adultmale','adultfemale','famadult','famwork','work','workmale','workfemale')) samp$wt = samp$pcwt
      
      #Get income
      if(sname == 'hh') samp$inc = samp$hhinc
      if(sname %in% c('fam','famprime','famwork','famadult')) samp$inc = samp$ftotinc
      if(sname %in% c('prime','primemp','primemale','primefemale','adult','adultmale','adultfemale','work','workmale','workfemale')) samp$inc = samp$inctot
      
      #Drop negative incomes (or should I set to zero?)
      #samp[samp$inc <0, 'inc'] = 0
      samp = filter(samp, inc >= 0)
      
      #Get sample of overall distribution for speed
      # natincsamp = base::sample(samp$inc, prob = samp$wt, size = 100000, replace = T)
      
      #Get national percentiles
      natpcts = Hmisc::wtd.quantile(samp$inc, weights = samp$wt, probs = 1:100 / 100)
      
      #Get duplicates
      natdupes = natpcts[duplicated(natpcts)]
      
      #Get number of duplicates in each case
      dupecnt = table(natdupes)
      
      #Get names 
      pctnames = NULL
      pstart = 'p0to'
      for(pct in 1:99){
        if(natpcts[pct] == natpcts[pct+1]) next()# %in% natdupes) 
        
        pname = paste(pstart,pct,sep = '')
        pctnames = c(pctnames, pname)
        pstart = paste('p',pct,'to',sep = '')
        
      }
      pname = paste(pstart,100,sep = '')
      pctnames = c(pctnames, pname)
      
      #Get deduplicated list for slicing
      pctcuts = c(-1,natpcts[!duplicated(natpcts)]+.1)
  
      #Assign people to national percentiles
      samp$pctile = cut(samp$inc,breaks = pctcuts,labels = pctnames)
      
      #Get count and mean and median by percentiles
      natstats = samp %>% group_by(pctile) %>% summarise(cnt = sum(wt), mean = wtd.mean(inc, weights = wt), median = wtd.quantile(inc, weights = wt,probs = .5)) %>% data.frame()
      
      #get top and bottom percentiles
      natstats$botpct = str_extract(natstats$pctile,'p[0-9][0-9]?') %>% str_replace('p','') %>% as.numeric()
      natstats$toppct = str_extract(natstats$pctile,'to[0-9][0-9]?[0-9]?') %>% str_replace('to','') %>% as.numeric()
      natstats$pcts = natstats$toppct - natstats$botpct
      
      #Now make 100 row dataset with the count, mean, and median for each percentile
      nat100stats = NULL
      for(p in 1:100) {
        rowint = filter(natstats, toppct >= p, botpct < p)
        upperlim = natpcts[p]
        stopifnot(nrow(rowint) < 2)
        if(nrow(rowint)==0) {
          pcnt = 0
          pmean = 0
          pmed = 0
          ppct = 0
        }
        if(nrow(rowint) == 1){
          pmean = rowint$mean
          pmed = rowint$median
          pcnt = rowint$cnt / rowint$pcts
          ppct = pcnt / sum(natstats$cnt)
        }
        nat100stats = rbind(nat100stats, c(p,pcnt,ppct,pmean,pmed,upperlim,fullyr))
      }
      nat100stats = data.frame(nat100stats)
      names(nat100stats) = c('p','cnt','pct','mean','med','upperlim','year')
      write.csv(nat100stats,paste('output/ipums/',fname,'/dta/',sname,'/',fname,'_pct_nat_',sname,'_',fullyr,'.csv',sep = ''),row.names = F)
        
      #Now make a 100 * cities row dataset with same stats for each city
      citystats = samp %>% group_by(city,cityname,pctile) %>% summarise(cnt = sum(wt), mean = wtd.mean(inc, weights = wt), median = wtd.quantile(inc, weights = wt,probs = .5)) %>% data.frame()
      
      #get top and bottom percentiles
      citystats$botpct = str_extract(citystats$pctile,'p[0-9][0-9]?') %>% str_replace('p','') %>% as.numeric()
      citystats$toppct = str_extract(citystats$pctile,'to[0-9][0-9]?[0-9]?') %>% str_replace('to','') %>% as.numeric()
      citystats$pcts = citystats$toppct - citystats$botpct
      
      #Loop through each city and percentile
      city100stats = NULL
      for(ci in unique(citystats$city)) {
        cstats = NULL
        cpop = filter(citystats, city == ci) %>% select(cnt) %>% sum()
        print(ci)
        for(p in 1:100){
          # baset = Sys.time()
          rowint = filter(citystats, toppct >= p, botpct < p, city == ci)
          # print(Sys.time() - baset)
          numrows = dim(rowint)[1]
          # print(Sys.time() - baset)
          # stopifnot(numrows < 2) #The nrows take quite a bit of time, so stopping
          # print(Sys.time() - baset)
          if(numrows==0) {
            ciname = ''
            pcnt = 0
            pmean = NA
            pmed = NA
            ppct = 0
          } else {
          # print(Sys.time() - baset)
          # if(numrows == 1){
            # print(Sys.time() - baset)
            ciname = rowint$cityname
            pmean = rowint$mean
            pmed = rowint$median
            pcnt = rowint$cnt / rowint$pcts
            # print(Sys.time() - baset)
            ppct = pcnt / cpop
            # print(Sys.time() - baset)
          }
          # print(Sys.time() - baset)
          cstats = rbind(cstats, c(ci,ciname,p,pcnt,ppct,pmean,pmed,fullyr))
          # print(Sys.time() - baset)
          
        }
        city100stats = rbind(city100stats,cstats)
      }
      
      city100stats = data.frame(city100stats)
      names(city100stats) = c(fname,cname,'p','cnt','pct','mean','med','year')
      write.csv(city100stats,paste('output/ipums/',fname,'/dta/',sname,'/',fname,'_pct_city_',sname,'_',fullyr,'.csv',sep = ''),row.names = F)
      
      
      
      
      
    } #sample
  }#year

  
}#variable
    