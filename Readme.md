Readme explaining code for 

Prep code
msalookups.R - makes crosswalks from PUMAs to 2013 MSAs and 1990 CZs
map_key_county_msa.R - Makes a list mapping county FIPS to the lowercase county name for mapping with the maps package. Also includes MSA and CZ codes

GDP per capita analysis
gdp_make_msa.R - Takes BEA county GDP data and converts to an inflated MSA GDP per capita dataset
gdp_make_cz.R - Takes BEA county GDP data and converts to an inflated CZ GDP per capita dataset

gdp_analyze_cz.R - Takes output from gdp_make_cz.R and creates graphs and tables of CZ and MSA personal income per capita trends over time. Output stored in output/gdp/[city]/devstats
gdp_maps_msa.R - Takes output from gdp_make_msa.R and maps it. Output stored in output/gdp/msa/maps
gdp_maps_cz.R - Takes output from gdp_make_cz.R and maps it. Output stored in output/gdp/msa/cz

IPUMS distribution analysis
ipumsprep.R - Takes huge raw IPUMS data file and converts it to MSA and CZ files for each decade 1980, 90, 2000, 2010, and 2015

ipums_make_msas.R - Takes MSA and CZ IPUMS data by decade and computes means, variances, and income distributions for each metro for a variety of samples. Saves results in output/ipums/[metrotype]/dta

ipums_analyze_msas_meanmedian.R - Takes output from ipums_make_msas.R and computes various statistics about mean and median incomes across MSAs and CZs over time. outputs into output/ipums/[metro]/devstats/

ipums_segregation.R - Computes rank-order income segregation across metros. Outputs into output/ipums/[metro]/segregation

ipums_percentiles.R - Computes national income percentiles for various income measures in IPUMS. Distributes duplicate values across buckets evenly so there are always 100 buckets. Computes the mean and median income, count, and percentage of total population in each national percentile, both for the nation and for each metro. Outputs into output/ipums/[metro]/dta/[sample]

ipums_counterfactuals.R - Constructs counterfactual scenarios based on the output of ipums_percentiles.R. Plays . Does buckets of various sizes ranging from 1-10 percentiles.

ipums_droptop.R - Constructs counterfactuals where I drop the top 1, 5, and 10% of the national income distribution and recompute the various devstats

Not current: 

czlookups.R - had made crosswalks from PUMAs to CZs
gdp_analyze_msa.R - Takes output from gdp_make_msa.R and creates graphs and tables of MSA GDP per capita trends over time. Output stored in output/gdp/msa/devstats