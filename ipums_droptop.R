#Try dropping top 1, 2, 5, 10% and recomputing SD etc

library(dplyr)
library(Hmisc)
library(maps)
library(stringr)
library(RColorBrewer)
library(scales)
library(readstata13)
library(ggplot2)
library(reshape2)
library(ggthemes)

setwd('~/projects/regionineq/')

#Bring in CPI
cpirs = read.dta13('~/projects/reference/cpi/cpirs.dta')
cpi15 = cpirs[cpirs$year == 2015,'rate']

#Crosswalk from MSAs to counties in the maps
msas = read.csv('data/mable/cntymsakey.csv')
msas$reg = msas$polyname

cntys = map_data('county')
cntys$reg = paste(cntys$region, cntys$subregion, sep = ',')
cntys = merge(cntys,msas,by = 'reg')

#Going to loop over MSAs vs CZs
varnames = c('msa','czone')
filenames = c('msa','cz')
cleannames = c('msaname','czname')
citynames = c('MSA','Commuting Zone')

for(i in 1:length(varnames)){  # 2){ #  
  vname = varnames[i]
  fname = filenames[i]
  cname = cleannames[i]
  cityname = citynames[i]
  
  cntys$city = cntys[,vname]
  
  # natldta = read.csv(paste('output/ipums/',fname,'/natinfo_',fname,'.csv',sep = ''))
  # natldta$year = natldta$fullyear
  
  snames = c('hh','fam','famwork','famprime','prime','primemale','primefemale','work','workmale','workfemale')#,'primemale','famwork') #prime','fam','famprime',','adult','adultmale','adultfemale','famadult)
  tnames = c('household','family','working-age family','prime-age family','prime-age adult','prime-age male','prime-age female','working age adult','working-age male','working-age female')#,'prime-age male','working-age family')#,'adult','adult male','adult female','adult family')prime-age adult','family','prime-age family',''prime-age female','
  
  dir.create(paste('output/ipums/',fname,'/counterfactuals',sep = ''))
  
  dtsig = NULL
  dtbeta = NULL
  
  for(s in 1:length(snames)) {
    sname = snames[s]
    tname = tnames[s]
    
    dir.create(paste('output/ipums/',fname,'/counterfactuals/',sname,sep = ''))
      
      
      #Get overall national mean and median
      # natstats = filter(natldta,samp == sname)
      
      #Make dataframe to store national information from each sample
      natinfo80 = read.csv(paste('output/ipums/',fname,'/dta/',sname,'/',fname,'_pct_nat_',sname,'_1980.csv',sep = ''))
      natinfo90 = read.csv(paste('output/ipums/',fname,'/dta/',sname,'/',fname,'_pct_nat_',sname,'_1990.csv',sep = ''))
      natinfo00 = read.csv(paste('output/ipums/',fname,'/dta/',sname,'/',fname,'_pct_nat_',sname,'_2000.csv',sep = ''))
      natinfo10 = read.csv(paste('output/ipums/',fname,'/dta/',sname,'/',fname,'_pct_nat_',sname,'_2010.csv',sep = ''))
      natinfo15 = read.csv(paste('output/ipums/',fname,'/dta/',sname,'/',fname,'_pct_nat_',sname,'_2015.csv',sep = ''))
      
      natinfo = rbind(natinfo80,natinfo90,natinfo00, natinfo10, natinfo15)
      
      #Make city year by year data frame
      city80 = read.csv(paste('output/ipums/',fname,'/dta/',sname,'/',fname,'_pct_city_',sname,'_1980.csv',sep = ''))
      city90 = read.csv(paste('output/ipums/',fname,'/dta/',sname,'/',fname,'_pct_city_',sname,'_1990.csv',sep = ''))
      city00 = read.csv(paste('output/ipums/',fname,'/dta/',sname,'/',fname,'_pct_city_',sname,'_2000.csv',sep = ''))
      city10 = read.csv(paste('output/ipums/',fname,'/dta/',sname,'/',fname,'_pct_city_',sname,'_2010.csv',sep = ''))
      city15 = read.csv(paste('output/ipums/',fname,'/dta/',sname,'/',fname,'_pct_city_',sname,'_2015.csv',sep = ''))

      city = rbind(city80, city90, city00, city10, city15)
      
      #Add city variable
      city$city = as.character(city[,fname])
      
      #Do mean and median
      stattypes = c('med','mean')
      statnames = c('median','mean')
      for(statnum in 1:length(stattypes)){
        st = stattypes[statnum]
        stname = statnames[statnum]
        
        print(paste(vname,sname,stname))
        
        
        natinfo$stat = natinfo[,st]
        city$stat = city[,st]
        
        #Compute national and city statistic to start
        # natmeans = natstats[,c('fullyear',paste('m',stat,sep = ''))]
        if(st == 'mean'){
          natmeans = natinfo %>% group_by(year) %>% summarise(natstat = wtd.mean(stat, weights = cnt)) %>% data.frame()
          citymeans = city %>% group_by(city, year) %>% summarise(pop = sum(cnt), stat = wtd.mean(stat, weights = cnt)) %>% data.frame()
        }
        if(st == 'med'){
          natmeans = natinfo %>% group_by(year) %>% summarise(natstat = wtd.quantile(stat, weights = cnt,probs = .5)) %>% data.frame()
          citymeans = city %>% group_by(city, year) %>% summarise(pop = sum(cnt), stat = wtd.quantile(stat, weights = cnt,probs = .5)) %>% data.frame()
        }

      for(droptop in c(1,5,10)){
          
        #Make grouped percentiles
        natdropped = filter(natinfo, p <= 100-droptop)

        #Make national means for each year after dropping
        if(st == 'mean'){
          natmeansdr = natdropped %>% group_by(year) %>% summarise(stat = wtd.mean(stat, weights = cnt)) %>% data.frame()
        }
        if(st == 'med'){
          natmeansdr = natdropped %>% group_by(year) %>% summarise(natstat = wtd.quantile(stat, weights = cnt,probs = .5)) %>% data.frame()
        }
        names(natmeansdr) = c('year',paste('natstat_dr',droptop,sep = ''))
        
        #Combine with overall means
        natmeans = merge(natmeans,natmeansdr,by = 'year')
        
        #Make city means for each year after dropping
        citydropped = filter(city, p <= 100-droptop)
        if(st == 'mean'){
          citymeansdr = citydropped %>% group_by(city, year) %>% summarise(pop = sum(cnt), stat = wtd.mean(stat, weights = cnt)) %>% data.frame()
        }
        if(st == 'med'){
          citymeansdr = citydropped %>% group_by(city, year) %>% summarise(pop = sum(cnt), stat = wtd.quantile(stat, weights = cnt,probs = .5)) %>% data.frame()
        }
        names(citymeansdr) = c('city','year',paste('pop_dr',droptop,sep = ''),paste('stat_dr',droptop,sep = ''))
      
        #Combine with overall means
        citymeans = merge(citymeans, citymeansdr, by = c('city','year'))
      }
      
      #Compute city means relative to national means
      citymeans = merge(citymeans, natmeans, by = 'year')
      citymeans$rel = citymeans$stat / citymeans$natstat
      for(droptop in c(1,5,10)){
        citymeans[,paste('rel_dr',droptop,sep = '')] = citymeans[,paste('stat_dr',droptop,sep = '')] / citymeans[,paste('natstat_dr',droptop,sep = '')] 
      }
        
      #Now compute variation statistics dropping various amounts
      citystats = citymeans %>% group_by(year) %>% 
        summarise(
          sd = sqrt(wtd.var(rel,weights = pop)),
          sd1 = sqrt(wtd.var(rel_dr1,weights = pop_dr1)),
          # sd2 = sqrt(wtd.var(rel_dr2,weights = pop_dr2)),
          sd5 = sqrt(wtd.var(rel_dr5,weights = pop_dr5)),
          sd10 = sqrt(wtd.var(rel_dr10,weights = pop_dr10)),
          # sd20 = sqrt(wtd.var(rel_dr20,weights = pop_dr20)),
          p10d = wtd.quantile(rel,weights = pop, probs = .1),
          p10d1 = wtd.quantile(rel_dr1,weights = pop_dr1, probs = .1),
          # p10d2 = wtd.quantile(rel_dr2,weights = pop_dr2, probs = .1),
          p10d5 = wtd.quantile(rel_dr5,weights = pop_dr5, probs = .1),
          p10d10 = wtd.quantile(rel_dr10,weights = pop_dr10, probs = .1),
          # p10d20 = wtd.quantile(rel_dr20,weights = pop_dr20, probs = .1),
          p25d = wtd.quantile(rel,weights = pop, probs = .25),
          p25d1 = wtd.quantile(rel_dr1,weights = pop_dr1, probs = .25),
          # p25d2 = wtd.quantile(rel_dr2,weights = pop_dr2, probs = .25),
          p25d5 = wtd.quantile(rel_dr5,weights = pop_dr5, probs = .25),
          p25d10 = wtd.quantile(rel_dr10,weights = pop_dr10, probs = .25),
          # p25d20 = wtd.quantile(rel_dr20,weights = pop_dr20, probs = .25),
          p75d = wtd.quantile(rel,weights = pop, probs = .75),
          p75d1 = wtd.quantile(rel_dr1,weights = pop_dr1, probs = .75),
          # p75d2 = wtd.quantile(rel_dr2,weights = pop_dr2, probs = .75),
          p75d5 = wtd.quantile(rel_dr5,weights = pop_dr5, probs = .75),
          p75d10 = wtd.quantile(rel_dr10,weights = pop_dr10, probs = .75),
          # p75d20 = wtd.quantile(rel_dr20,weights = pop_dr20, probs = .75),
          p90d = wtd.quantile(rel,weights = pop, probs = .9),
          p90d1 = wtd.quantile(rel_dr1,weights = pop_dr1, probs = .9),
          # p90d2 = wtd.quantile(rel_dr2,weights = pop_dr2, probs = .9),
          p90d5 = wtd.quantile(rel_dr5,weights = pop_dr5, probs = .9),
          p90d10 = wtd.quantile(rel_dr10,weights = pop_dr10, probs = .9)
          # p90d20 = wtd.quantile(rel_dr20,weights = pop_dr20, probs = .9)
        ) %>% data.frame()
      
      for(droptop in c('',1,5,10)){
        citystats[,paste('iqrd',droptop,sep = '')] = citystats[,paste('p75d',droptop,sep = '')] - citystats[,paste('p25d',droptop,sep = '')] 
        citystats[,paste('p19d',droptop,sep = '')] = citystats[,paste('p90d',droptop,sep = '')] - citystats[,paste('p10d',droptop,sep = '')] 
      }
      
      #Plot various ratios over time
      varis = c('sd','iqrd','p19d')
      varinames = c('coefficient of variation','inter-quartile range','90-10 range')
      
      sigstats = citystats[,c('year',
          grep('sd',names(citystats),value = T),
          grep('iqr',names(citystats),value = T),
          grep('p19',names(citystats),value = T))]
      sigstats$samp = sname
      sigstats$stat = st
      dtsig = rbind(dtsig, sigstats)
      
      for(vi in 1:length(varis)){
        vari = varis[vi]
        variname = varinames[vi]
        pdta = citystats[,c('year',grep(vari,names(citystats),value = T))]
        pdta = melt(pdta, id.vars = 'year')
        pdta$variable = factor(pdta$variable,levels = paste(vari,c('',1,5,10),sep = ''),labels = c('Full sample',paste('Dropping top ',c(1,5,10),'%',sep = '')))

        #Do raw
        pdf(paste('output/ipums/',fname,'/counterfactuals/',sname,'/dt_',sname,'_',st,'_sig_',vari,'.pdf',sep = ''))
        gplt = ggplot(pdta, aes(x = year, y = value, lty = variable)) +
          geom_line() +
          geom_point() +
          theme_bw() +
          scale_linetype_discrete(name = '') +
          scale_x_continuous(name = 'Year') +
          scale_y_continuous(name = '',labels = percent,limits = c(0,1.1* max(pdta$value))) +
          ggtitle(paste('Cross-',cityname,' ',variname,' of ',stname,' ',tname,'\nincome over time, dropping top income groups',sep = ''))
        print(gplt)
        dev.off()
        
        #Do growth relative to self in 1980
        pdta80 = filter(pdta,year == 1980)
        ndta = merge(pdta,pdta80,by = c('variable'),suffixes = c('','_80'))
        ndta$growth = ndta$value / ndta$value_80
        
        #Make plot
        pdf(paste('output/ipums/',fname,'/counterfactuals/',sname,'/dt_',sname,'_',st,'_sig_gr_',vari,'_',sname,'.pdf',sep = ''))
        gplt = ggplot(ndta, aes(x = year, y = growth, lty = variable, group = variable)) +
          geom_line() +
          geom_point() +
          theme_bw() +
          scale_linetype_discrete(name = '') +
          scale_x_continuous(name = 'Year') +
          scale_y_continuous(name = '',labels = percent,limits = c(.9*min(ndta$growth),1.1* max(ndta$growth))) +
          ggtitle(paste('Growth in cross-',cityname,' ',variname,' of\n',stname,' ',tname,' income over time, dropping top income groups',sep = ''))
        print(gplt)
        dev.off()
      }
      
      #Now do beta convergence -- annual growth rate 1980-2015 vs level 2015
      city1980 = filter(citymeans, year == 1980)
      citygr = merge(citymeans, city1980, by = 'city', suffixes = c('','_1980'))
      citygr = filter(citygr, year == 2015)
      
      dts = c('','_dr1','_dr5','_dr10')
      dtnames = c('','dropping top 1%','dropping top 5%','dropping top 10%')
      betas = c(sname,st)
      
      for(dt in 1:length(dts)){ #'_dr2',,'_dr20'
        droptop = dts[dt]
        dtname = dtnames[dt]
        citygr$curmean = citygr[,paste('stat',droptop,sep = '')]
        citygr$mean1980 = citygr[,paste('stat',droptop,'_1980',sep = '')]
        citygr$curpop = citygr[,paste('pop',droptop,sep = '')]
        
        citygr$anngr = (citygr$curmean / citygr$mean1980) ^ (1/(2015-1980)) - 1
      
        #Fit linear model
        mod1 = lm(formula = anngr ~ mean1980,weights = curpop,data = filter(citygr,mean1980 >0)) #Note I drop if they had literally zero median income in 1980
        betas = c(betas,mod1$coefficients[2])
        
        #Plot annual growth rate vs
        pdf(paste('output/ipums/',fname,'/counterfactuals/',sname,'/dt_',sname,'_',st,'_beta',droptop,'.pdf',sep = ''))
        gplt = ggplot(citygr, aes(x = mean1980, y = anngr,size = curpop)) +
          geom_point(alpha = .3) +
          theme_bw() +
          scale_x_continuous(labels = dollar, name = paste('1980',cityname,stname,'income'))+
          scale_y_continuous(labels = percent, name = paste('Annualized growth rate in',stname,'income, 1980-2015')) +
          geom_abline(intercept = mod1$coefficients[1], slope = mod1$coefficients[2],lty = 2) +
          scale_size_continuous(range = c(0,5), labels = comma, name = paste(cityname, '\nPopulation')) +
          ggtitle(paste('Beta convergence of',cityname,stname,tname,'income\n1980-2015,',dtname))
        
        print(gplt)
        dev.off()
        
        
        }
      dtbeta = rbind(dtbeta, betas)
      
      } #stat
  } #samp
  dtbeta = data.frame(dtbeta)
  names(dtbeta) = c('samp','stat','d0','d1','d5','d10')
  write.csv(dtbeta,paste('output/ipums/',fname,'/counterfactuals/dt_beta_',fname,'.csv',sep = ''),row.names = F)
  
  dtsig = data.frame(dtsig)
  write.csv(dtsig,paste('output/ipums/',fname,'/counterfactuals/dt_sigma_',fname,'.csv',sep = ''),row.names = F)
  
  
} #var
