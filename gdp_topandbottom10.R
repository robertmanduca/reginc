#Take output of ipums_percentiles and use to to a more complete version of the counterfactuals
#First bring in cities and national income stats, rbind them into two files
#Merge 

library(dplyr)
library(Hmisc)
library(maps)
library(stringr)
library(RColorBrewer)
library(scales)
library(readstata13)
library(ggplot2)
library(reshape2)
library(ggthemes)

setwd('~/projects/regionineq/')

dta = read.csv('output/dta/cz_gdppc.csv')
d8 = filter(dta, year == 1980)
cut8090 =wtd.quantile(d8$gdprel, weights = d8$pop,probs = .95)
cut8010 = wtd.quantile(d8$gdprel, weights = d8$pop,probs = .05)
cut8090 / cut8010

wtd.quantile(d8$gdprel, weights = d8$pop,probs = .9)
wtd.quantile(d8$gdprel, weights = d8$pop,probs = .1)

d15 = filter(dta, year == 2015)
cut1590 =wtd.quantile(d15$gdprel, weights = d15$pop,probs = .95)
cut1510 = wtd.quantile(d15$gdprel, weights = d15$pop,probs = .05)
cut1590 / cut1510

wtd.quantile(d15$gdprel, weights = d15$pop,probs = .9)
wtd.quantile(d15$gdprel, weights = d15$pop,probs = .1)


#Weird

wtd.mean(filter(d8, gdprel >= cut8090)$gdprel, weights = filter(d8, gdprel >= cut8090)$pop)  #1.423969
wtd.quantile(filter(d8, gdprel >= cut8090)$gdprel, weights = filter(d8, gdprel >= cut8090)$pop, probs = .5)  #1.423969
median(filter(d8, gdprel >= cut8090)$gdprel)  #1.314567


sum(d8$gdp)


#How much of income is in the census
c80inc = filter(dta, year == 1979)
cdta = read.csv('output/ipums/cz/dta/hh/cz_pct_city_hh_1980.csv')
cdta$total = cdta$cnt * cdta$mean
